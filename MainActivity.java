package com.innovators.doctortracker;

import android.R.id;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
       final Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
       final Button welcomeButton ;
        welcomeButton = (Button) findViewById(R.id.welcomeActivityButton);
        
        welcomeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				v.startAnimation(animScale);
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						Intent intent = new Intent(MainActivity.this, LocationActivity.class);
						startActivity(intent);
					}
				},1000);
				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
