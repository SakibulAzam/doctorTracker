package com.innovators.doctortracker;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore.Audio.Radio;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class LocationActivity extends Activity {
  
	public static int a;
	public static int x;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);
		
		RadioGroup catagoryGroup = (RadioGroup) findViewById(R.id.catagoryRadioGroup);
		RadioGroup cityGroup = (RadioGroup) findViewById(R.id.cityRadioGroup);
		Button goToAreaActivityButton = (Button) findViewById(R.id.goToAreaActivityButton);
		
		
		final RadioButton dhakaButton = (RadioButton) findViewById(R.id.dhakaRadio);
		final RadioButton chittagongButton = (RadioButton) findViewById(R.id.chittagongRadio);
		final RadioButton rajshahiButton = (RadioButton) findViewById(R.id.rajshahiRadio);
		final RadioButton khulnaButton = (RadioButton) findViewById(R.id.khulnaRadio);
		final RadioButton barishalButton = (RadioButton) findViewById(R.id.barishalRadio);
		final RadioButton sylhetButton = (RadioButton) findViewById(R.id.sylhetRadio);
		final RadioButton rangpurButton = (RadioButton) findViewById(R.id.rangpurRadio);
		
		
		final RadioButton gpButton = (RadioButton) findViewById(R.id.gpRadio);
		final RadioButton skinButton = (RadioButton) findViewById(R.id.skinRadio);
		final RadioButton entButton = (RadioButton) findViewById(R.id.entRadio);
		final RadioButton eyeButton = (RadioButton) findViewById(R.id.eyeRadio);
		final RadioButton medicineButton = (RadioButton) findViewById(R.id.MedicineRadio);
		final RadioButton paediatricButton = (RadioButton) findViewById(R.id.paediatricsRadio);
		final RadioButton dentistryButton = (RadioButton) findViewById(R.id.dentistryRadio);
		final RadioButton cardiologyButton = (RadioButton) findViewById(R.id.cardiologyRadio);
		
		
		
		
		cityGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				if (dhakaButton.isChecked()) {
					a = 11;
				}
				if (chittagongButton.isChecked()) {
					a = 12 ;
				}
				if (rajshahiButton.isChecked()) {
					a = 13;
				}
				if (khulnaButton.isChecked()) {
					a = 14;
				}
				if (barishalButton.isChecked()) {
					a = 15;
				}
				if (sylhetButton.isChecked()) {
					a = 16;
				}
				if (rangpurButton.isChecked()) {
					a = 17;
				}
			}
		});
		
		catagoryGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				if (gpButton.isChecked()) {

					x = 21;
				}
				if (skinButton.isChecked()) {
                    x = 22;
				}
				if (entButton.isChecked()) {
					x = 23;
				}
				if (eyeButton.isChecked()) {
					x= 24;
				}
				if (medicineButton.isChecked()) {
					x = 25;
				}
				if (paediatricButton.isChecked()) {
					x = 26;
				}
				if (dentistryButton.isChecked()) {
					x = 27;
				}
				if (cardiologyButton.isChecked()) {
					x = 28;
				}

			}
		});
		goToAreaActivityButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LocationActivity.this, AreaActivity.class);
				startActivity(intent);
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.location, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
